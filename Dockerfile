# Use a base image with JDK and Gradle installed
FROM adoptopenjdk:11-jdk-hotspot AS build

# Set the working directory inside the container
WORKDIR /app

# Copy the Gradle configuration files
COPY settings.gradle.kts build.gradle.kts /app/
COPY gradle /app/gradle

# Download and cache the Gradle distribution
RUN ./gradle/wrapper/gradle-wrapper.jar --version

# Copy the source code
COPY src /app/src

# Build the application
RUN ./gradlew build --no-daemon

# Use a lighter base image for the runtime
FROM adoptopenjdk:11-jre-hotspot

# Set the working directory inside the container
WORKDIR /app

# Copy the built JAR file from the build stage
COPY --from=build /app/build/libs/*.jar /app/app.jar

# Expose the port on which the Spring Boot application listens
EXPOSE 8080

# Define the command to run the application
CMD ["java", "-jar", "app.jar"]

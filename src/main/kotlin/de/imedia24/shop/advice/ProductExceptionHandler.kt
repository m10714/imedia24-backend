package de.imedia24.shop.advice

import de.imedia24.shop.exception.ErrorMessage
import de.imedia24.shop.exception.FieldError
import de.imedia24.shop.exception.ProductNotFoundException
import org.slf4j.LoggerFactory

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.client.HttpClientErrorException.BadRequest
import org.springframework.web.context.request.WebRequest
import java.time.LocalDateTime

@ControllerAdvice
class ProductExceptionHandler{

    private val logger = LoggerFactory.getLogger(ProductExceptionHandler::class.java)!!

    @ExceptionHandler(ProductNotFoundException::class)
    @ResponseBody
    fun handleProductNotFoundException(
        ex: ProductNotFoundException,
        request: WebRequest
    ): ResponseEntity<ErrorMessage> {
        logger.error("Handle error $ex")
        val errorResponse = ErrorMessage(
            HttpStatus.NOT_FOUND.value(),
            HttpStatus.NOT_FOUND.name,
            ex.message ?: "Product not found",
            LocalDateTime.now(),
            request.getDescription(false)
        )
        return ResponseEntity(errorResponse, HttpStatus.NOT_FOUND)
    }

    @ExceptionHandler(BadRequest::class)
    @ResponseBody
    fun handleBadRequestException(ex: Exception, request: WebRequest): ResponseEntity<ErrorMessage> {
        logger.error("Handle error $ex")
        val errorResponse = ErrorMessage(
            HttpStatus.BAD_REQUEST.value(),
            HttpStatus.BAD_REQUEST.name,
            ex.message?:"Bad request error",
            LocalDateTime.now(),
            request.getDescription(false)
        )
        return ResponseEntity(errorResponse, HttpStatus.BAD_REQUEST)
    }
    
    @ExceptionHandler(Exception::class)
    @ResponseBody
    fun handleGenericException(ex: Exception, request: WebRequest): ResponseEntity<ErrorMessage> {
      logger.error("Handle error $ex")
        val errorResponse = ErrorMessage(
            HttpStatus.INTERNAL_SERVER_ERROR.value(),
            HttpStatus.INTERNAL_SERVER_ERROR.name,
             "Internal server error",
            LocalDateTime.now(),
            request.getDescription(false)
        )
        return ResponseEntity(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR)
    }





    private fun createValidationErrorResponse(bindingResult: BindingResult, request: WebRequest): ErrorMessage {
        val errorResponse = ErrorMessage(
            HttpStatus.BAD_REQUEST.value(),
            HttpStatus.BAD_REQUEST.name,
            "Validation failed",
            LocalDateTime.now(),
            request.getDescription(false)
        )

        bindingResult.fieldErrors.forEach { fieldError ->
            val fieldErrorDto = FieldError(fieldError.field, fieldError.defaultMessage ?: "Validation error")
            errorResponse.fieldErrors.add(fieldErrorDto)
        }

        return errorResponse
    }

}
package de.imedia24.shop.service

import de.imedia24.shop.domain.product.ProductCreateRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductUpdateRequest
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable


interface ProductService {
    fun findProductBySku(sku: String): ProductResponse?
    fun findProductBySkus(sku: Set<String>): List<ProductResponse>?
    fun findAllByPageable(pageable: Pageable) : Page<ProductResponse>
    fun createProduct(product: ProductCreateRequest): ProductResponse
    fun update(product: ProductUpdateRequest): ProductResponse
    fun delete(sku: String)
}
package de.imedia24.shop.service.impl

import de.imedia24.shop.exception.ProductNotFoundException
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductCreateRequest
import de.imedia24.shop.domain.product.ProductCreateRequest.Companion.toEntity
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import de.imedia24.shop.domain.product.ProductUpdateRequest
import de.imedia24.shop.domain.product.ProductUpdateRequest.Companion.toEntity
import de.imedia24.shop.service.ProductService
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import kotlin.streams.toList

@Service
class ProductServiceImpl(private val productRepository: ProductRepository):ProductService {

    override fun findProductBySku(sku: String): ProductResponse? {
       val productEntity = productRepository.findBySku(sku) ?: throw ProductNotFoundException("Product : $sku not found !")
        return productEntity.toProductResponse()
    }


    override fun findProductBySkus(sku: Set<String>): List<ProductResponse> {
        val productEntity = productRepository.findAllBySkuIn(sku)
        return productEntity
            .stream()
            .map { element-> element.toProductResponse() }
            .toList()
    }

    override fun findAllByPageable(pageable: Pageable) : Page<ProductResponse>{
        val productPage = productRepository.findAll(pageable)
        val productsResponse = productPage
            .content
            .stream()
            .map { element-> element.toProductResponse() }
            .toList()
        return PageImpl(productsResponse,pageable, productPage.totalElements)
    }
    override fun createProduct(product: ProductCreateRequest):ProductResponse {
      val created = productRepository.save(product.toEntity())
        return created.toProductResponse()
    }
    override fun update(product: ProductUpdateRequest):ProductResponse {
        productRepository.findBySku(product.sku) ?: throw ProductNotFoundException("Product : ${product.sku} not found !")
        val updated = productRepository.save(product.toEntity())
        return updated.toProductResponse()
    }
    override fun delete(sku: String) {
        productRepository.deleteById(sku)
    }
}

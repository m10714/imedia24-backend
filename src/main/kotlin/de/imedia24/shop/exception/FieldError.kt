package de.imedia24.shop.exception

class FieldError(val field: String, val message: String) {
}
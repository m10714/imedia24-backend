package de.imedia24.shop.exception

import java.time.LocalDateTime

class ErrorMessage(
    val statusCode: Int,
    val error: String,
    val message: String,
    val time: LocalDateTime,
    val request: String,
    val fieldErrors: MutableList<FieldError> = mutableListOf()
) {
}
package de.imedia24.shop.controller

import de.imedia24.shop.domain.product.ProductCreateRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductUpdateRequest
import de.imedia24.shop.service.impl.ProductServiceImpl
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpEntity
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class ProductController(private val productService: ProductServiceImpl) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!


    @PostMapping("/products", produces = ["application/json;charset=utf-8"])
    fun createProduct(
        @RequestBody product: ProductCreateRequest
    ): ResponseEntity<ProductResponse> {
        logger.info("Request to create product $product")
        val created = productService.createProduct(product  )
       return ResponseEntity
           .status(HttpStatus.CREATED)
           .body(created)
    }

    @PutMapping("/products", produces = ["application/json;charset=utf-8"])
    fun updateProduct(
        @RequestBody product:ProductUpdateRequest
    ): ResponseEntity<ProductResponse> {
        logger.info("Request update product $product")
        val updated = productService.update(product )
        return ResponseEntity
            .status(HttpStatus.CREATED)
            .body(updated)
    }

    @DeleteMapping("/products/{sku}", produces = ["application/json;charset=utf-8"])
    fun deleteProduct(
        @PathVariable("sku") sku: String
    ): HttpEntity<*> {
        logger.info("Request delete product $sku")
        productService.delete(sku )
        return ResponseEntity.EMPTY
    }
    @GetMapping("/products/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductBySku(
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for product $sku")

        val product = productService.findProductBySku(sku)
        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }
    @GetMapping("/products", produces = ["application/json;charset=utf-8"])
    fun findProductsBySku(
        @RequestParam skus:Set<String>
    ): ResponseEntity<List<ProductResponse>> {
        logger.info("Request for product $skus")
        val products = productService.findProductBySkus(skus)
        return ResponseEntity.ok(products)
    }
    @GetMapping("/products/search", produces = ["application/json;charset=utf-8"])
    fun findProductsByPageable(
        pageable: Pageable
    ): ResponseEntity<Page<ProductResponse>> {
        logger.info("Request for product $pageable")
        val productPage = productService.findAllByPageable(pageable)
        return ResponseEntity.ok(productPage)
    }
}

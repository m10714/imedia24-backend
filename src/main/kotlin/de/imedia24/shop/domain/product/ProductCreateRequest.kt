package de.imedia24.shop.domain.product

import de.imedia24.shop.db.entity.ProductEntity
import java.math.BigDecimal
import java.util.UUID

class ProductCreateRequest(
                               val name: String,
                               val description: String?,
                               val price: BigDecimal) {

    companion object {
        fun ProductCreateRequest.toEntity() = ProductEntity(
            name = name,
            description = description ?: "",
            price = price
        )
    }
}
package de.imedia24.shop.db.entity

import org.hibernate.annotations.SQLDelete
import org.hibernate.annotations.UpdateTimestamp
import org.hibernate.annotations.Where
import java.math.BigDecimal
import java.time.ZonedDateTime
import java.util.UUID
import javax.persistence.*

@Entity
@Table(name = "products")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE products SET deleted = true WHERE sku = ?")
data class ProductEntity(
    @Id
    @Column(name = "sku", nullable = false)
    val sku: String,

    @Column(name = "name", nullable = false)
    val name: String,

    @Column(name = "description")
    val description: String? = null,

    @Column(name = "price", nullable = false)
    val price: BigDecimal,

    @UpdateTimestamp
    @Column(name = "created_at", nullable = false, updatable = false)
    val createdAt: ZonedDateTime,

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    val updatedAt: ZonedDateTime,

    @Column(name = "deleted", nullable = false)
    var deleted: Boolean = false,

//    @Version
//    @Column(name = "version", nullable = false)
//    var version: Long = 0
){

    constructor(name: String, description: String?,price: BigDecimal) : this(
        UUID.randomUUID().toString(),
        name,
        description,
        price,
        ZonedDateTime.now(),
        ZonedDateTime.now(),
        false)

    constructor(sku: String,name: String, description: String?,price: BigDecimal) : this(
        sku,
        name,
        description,
        price,
        ZonedDateTime.now(),
        ZonedDateTime.now(),
        false)

    constructor() : this(
        "",
        "",
        "",
        BigDecimal.ZERO,
        ZonedDateTime.now(),
        ZonedDateTime.now(),
        false)

}
